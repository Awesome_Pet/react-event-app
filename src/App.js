import React from 'react';

import './App.css';
import {Header} from "./components/header/Header";
import {NavContainer} from "./components/nav/NavContainer";
import {SideContainer} from "./components/side/SideContainer";
import {MainLayout} from "./components/content/MainLayout";
import {Tooltip} from "./components/base/tooltip/Tooltip";
import {TooltipComponent} from "./components/base/tooltip/TooltipComponent";

/**
 * @return {boolean}
 */
function App() {


  return (

    <div className="App">
        <TooltipComponent hint="none"/>
        <Header/>
        <NavContainer/>
        <div className="contentContainer">
            <SideContainer/>
            <MainLayout/>
        </div>

    </div>
  );
}

export default App;
