import React from 'react'
import {Search} from "./Search";

export class SearchList extends React.Component {

    static propTypes = {
        items: Array
    };

    constructor(props) {
        super(props);
        this.state = {items: props.items || []};
    }

    onListChange = (valuie) => {
        this.setState({
            items: this.props.items.filter(item => item.indexOf(valuie) !== -1)
        });
    };

    render() {
        return (
            <div>
                <Search onChange={this.onListChange} value={"None"}/>
                <ul>
                    {
                        this.state.items.map((item) => {
                           return <li key={item}>{item}</li>
                        })
                    }
                </ul>
            </div>
        )
    }
}
