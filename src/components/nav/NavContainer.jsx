import React from 'react'
import {withRouter} from "react-router";
import {NavButton} from "./NavButton";
import styles from "./Nav.module.css"

export const NavContainer = withRouter(({location}) => {
    const navButtonsConf = [
        {label: "STREAM", navUrl: "/stream", isActive: true},
        {label: "INVITE", navUrl: "/invite"},
        {label: "INCOMING", navUrl: "/incoming"},
        {label: "PASSED", navUrl: "/passed"},
    ];

    const checkActiveLocation = (index) => {
        return location.pathname.indexOf(navButtonsConf[index].navUrl) !== -1;
    };

    return (
        <nav className={styles.navContainer}>
            {navButtonsConf.map((navConf, index) => {
                return <NavButton
                    key={navConf.label}
                    label={navConf.label}
                    navUrl={navConf.navUrl}
                    isActive={checkActiveLocation(index)}/>
            })}
        </nav>
    )

});
