import React from 'react'
import {withRouter} from "react-router";
import styles from "./Nav.module.css"
import PropTypes from "prop-types";
import {Button} from "../button";

export const NavButton = withRouter(({history, ...props}) => {
    const onClick = () => {
        history.push(props.navUrl);
    };
    return (
        <div className={styles.navButtonContainer}>
            <Button class={styles.navButton} text={props.label} onClick={onClick}/>
            {props.isActive &&
                <div className={styles.navButtonActive}/>
            }
        </div>
    )

});

NavButton.propTypes = {
    label: PropTypes.string.isRequired,
    isActive: PropTypes.bool,
    navUrl: PropTypes.string
};
