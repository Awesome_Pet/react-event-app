import React from 'react';
import PropTypes from "prop-types";

export class Button extends React.Component {

    render() {
        return (
            <div>
                <input type="button"
                       value={this.props.text}
                       className={this.props.class}
                       onMouseEnter={this.props.mouseOn}
                       onMouseLeave={this.props.mouseOut}
                       onClick={this.props.onClick}/>
            </div>
        )
    }
}

Button.propTypes = {
    text: PropTypes.string.isRequired,
    class: PropTypes.string,
    onClick: PropTypes.func,
    mouseOn: PropTypes.func,
    mouseOut: PropTypes.func,
};
