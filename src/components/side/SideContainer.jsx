import React from 'react'
import {ContentContainer} from "./ContentContainer";
import {SideActions} from "./sideContent/SideActions";

import './SideContainer.css'
import {ParticipantsContainer} from "./sideContent/participants/ParticipantsContainer";

export class SideContainer extends React.Component {
    render() {
        return (
            <aside className="sideContainer">
                <ContentContainer content={<SideActions/>} header="Actions"/>
                <ContentContainer content={<ParticipantsContainer/>} header="Participants"/>
            </aside>
        )
    }
}
