import React from 'react'
import PropTypes from 'prop-types'
export const ContentContainer = (props) => {
   return (
       <div className="sideContentContainer">
           <h5>{props.header}</h5>
           {props.content}
       </div>
   )
};

ContentContainer.propTypes = {
    content: PropTypes.element.isRequired,
    header: PropTypes.string.isRequired
};
