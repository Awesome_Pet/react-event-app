import React from 'react';
import './SideActions.css';
import {Button} from "../../button";


export class SideActions extends React.Component {
    render() {
        return (
            <div className="actions-container">

                <ul>
                    <li><Button text="Accept" class="action-button accept"/></li>
                    <li><Button text="Decline" class="action-button decline"/></li>
                    <li><Button text="Accept later" class="action-button later"/></li>
                </ul>
            </div>
        )
    }
}
