import React from 'react'
import styles from "./ParticipantsContainer.module.css"
import {ParticipantNav} from "./ParticipantNav";

const navFilter = [
    { label: "All", filter: "all", style: `${styles.navButton} ${styles.all}`},
    { label: "Accepted", filter: "accept", style: `${styles.navButton} ${styles.accept}`},
    { label: "Declined", filter: "declined", style: `${styles.navButton} ${styles.decline}`},
    { label: "Later", filter: "later", style: `${styles.navButton} ${styles.later}`}
];
export const ParticipantsContainer = () => {
    const applyFilter = type => event => {

    };
    return (
        <div className={styles.partiContainer}>
            <ul className={styles.partiNavList}>
                {navFilter.map( (filterConf, index) => (
                    <ParticipantNav key={index}
                                    value={filterConf.label}
                                    handler={applyFilter(filterConf.filter)}
                                    className={filterConf.style}
                    />
                    )
                )}
            </ul>
            <div className={styles.partiList}>
                <ul>
                    <li>John Doe</li>
                    <li>John Doe</li>
                    <li>John Doe</li>
                    <li>John Doe</li>
                    <li>John Doe</li>
                    <li>John Doe</li>
                    <li>John Doe</li>
                    <li>John Doe</li>
                    <li>John Doe</li>
                    <li>John Doe</li>
                    <li>John Doe</li>
                </ul>
            </div>
        </div>
    )
};
