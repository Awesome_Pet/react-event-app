import React from 'react'
import PropTypes from 'prop-types'
import {Button} from "../../../button";

export const ParticipantNav = (props) => {
    const onMouseEnterHandler = event => {
        window.dispatchEvent(new CustomEvent("showHint", {
            detail: {
                target: event,
                hint: props.value
            }
        }));
    };

    const onMouseLeaveHandler = event => {
        window.dispatchEvent(new CustomEvent("hideHint"))
    };

    return (
        <li><Button onClick={props.handler} class={props.className} text={props.count.toString()}
                    mouseOn={onMouseEnterHandler}
                    mouseOut={onMouseLeaveHandler}
        /></li>
    )
};

ParticipantNav.propTypes = {
    count: PropTypes.number,
    value: PropTypes.string.isRequired,
    handler: PropTypes.func.isRequired,
    className: PropTypes.string.isRequired,
};

ParticipantNav.defaultProps = {
    count: 0
};
