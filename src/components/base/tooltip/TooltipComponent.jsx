import React from 'react'
import styles from './Tooltip.module.css'

export class TooltipComponent extends React.Component {

    state = {
        style: {
            top: -300 + 'px',
            left: -300 + 'px',
        },
        text: "none"
    };

    constructor(props) {
        super(props);
        this.tooltipRef = React.createRef();
    }

    componentDidMount() {
        window.addEventListener("showHint", this.doShowHint);
        window.addEventListener("hideHint", this.doHideHint);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        const tooltipRect = this.tooltipRef.current.getBoundingClientRect();

        let x = this.state.targetX - tooltipRect.width / 2;
        let y = this.state.targetY;

        if (prevState.targetX !== this.state.targetX && this.state.targetY !== prevState.targetX) {
            this.setState( {
                style: {
                    top: y + window.scrollY + 'px',
                    left: x + window.scrollX + 'px'
                }})
        }

    }

    componentWillUnmount() {
        window.removeEventListener("showHint", this.doShowHint);
        window.removeEventListener("hideHint", this.doHideHint);
    }

    render() {
        return (
            <div ref={this.tooltipRef} className={styles.tooltipContainer} style={this.state.style}>
                <div className={styles.tooltipText}>
                    {this.state.text}
                </div>
            </div>
        )
    }

    doShowHint = event => {
        if (!this.tooltipRef.current) return;

        const targetRect = event.detail.target.currentTarget.getBoundingClientRect();

        this.setState({
            targetX: targetRect.x + targetRect.width / 2,
            targetY: targetRect.y - targetRect.height,
            text: event.detail.hint
        });
    };

    doHideHint = event => {
        this.setState({
            targetX: -300,
            targetY: -300
        });
    };
}
