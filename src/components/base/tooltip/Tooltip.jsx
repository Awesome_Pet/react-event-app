import React, {useState, useEffect} from 'react'
import PropTypes from 'prop-types'
import styles from './Tooltip.module.css'

export function Tooltip(props) {
    const [tooltipPos, updateTooltipPos] = useState({top: 0, left: 0});
    const [tooltipText, updateTooltipText] = useState("none");

    let tooltip = React.createRef();
    console.log("start");
    const doShowHint = event => {
        if (!tooltip.current) return;

        updateTooltipText(event.detail.hint);
        const targetRect = event.detail.target.currentTarget.getBoundingClientRect();

        const tooltipRect = tooltip.current.getBoundingClientRect();

        let x = targetRect.x + targetRect.width / 2 - tooltipRect.width / 2;
        let y = targetRect.y - targetRect.height;
        updateTooltipPos({
            top: y + window.scrollY + 'px',
            left: x + window.scrollX + 'px'
        });
    };

    const doHideHint = event => {
        updateTooltipPos({
            top: -300 + 'px',
            left: -300 + 'px'
        });
    };

    useEffect( () => {
        window.addEventListener("showHint", doShowHint);
        window.addEventListener("hideHint", doHideHint);

        return () => {
            window.removeEventListener("showHint", doShowHint);
            window.removeEventListener("hideHint", doHideHint);
        }
    });

    return (
        <div ref={tooltip} className={styles.tooltipContainer} style={tooltipPos}>
            <div className={styles.tooltipText}>
                {tooltipText}
            </div>
        </div>
    )
}
