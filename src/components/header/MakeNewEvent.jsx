import React, {Component} from 'react'
import {imagePack} from "../../helpers/images";
import PropTypes from "prop-types";

export class MakeNewEvent extends Component {
    static propTypes = {
      style: PropTypes.oneOfType([PropTypes.object, PropTypes.string]).isRequired,
      iconPath: PropTypes.string
    };
    render() {
        return (
            <input type="image"
                   name="image"
                   alt="Create new"
                   src={this.props.iconPath ||imagePack.CREATE_ICON}
                   className={this.props.style}/>
        )
    }
}
