import React, {Component} from 'react'
import styles from "./header.module.css"
import {MakeNewEvent} from "./MakeNewEvent";
export class Header extends Component{

    render() {
        return (
            <header>
                <div className={styles.headerContent}>
                    <img className={styles.userIcon} src="icons/icon.png" alt="user icon"/>
                    <p>User profile</p>
                </div>
                <div className={styles.headerContent}>
                    <MakeNewEvent style={styles.userIcon}/>
                </div>
            </header>
        )
    }
}
