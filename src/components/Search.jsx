import React from 'react'
import PropTypes from "prop-types"
export class Search extends React.Component{

    static propTypes = {
        onChange: PropTypes.func,
        value: PropTypes.string
    };

    state = {
        value: this.props.value
    };

    onFieldChange = () => {
        this.props.onChange(this.refs.search.value);
    };

    render() {
        return (
            <div>
                <input ref="search" type="text" defaultValue={this.state.value} onChange={this.onFieldChange}/>
            </div>
        )
    }
}
